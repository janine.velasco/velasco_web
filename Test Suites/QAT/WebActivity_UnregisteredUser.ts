<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WebActivity_UnregisteredUser</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ad4aa840-cff1-4696-ab46-cae15eca761e</testSuiteGuid>
   <testCaseLink>
      <guid>aa5b7389-d7d3-4ed0-b5de-30dfbd25fa11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC01 - User can view the components of landing page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d85aef48-a69f-44b5-ac79-5cc15c70d1cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC02 - User can view components under Women tab upon hovering</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80af874b-bd32-4c6c-bf44-d15e3b91397b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC03 - User can add items to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8452c22f-2d64-4208-a8ea-0a9200ca24bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC04.1 - User can sign up after adding items to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4910f02e-cfa8-48a6-aa19-8fdfdfd7841b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC05 - User can proceed to checkout payment via Payment by Check</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbafc99f-c540-4f8d-9491-ca083686413c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC06 - User can confirm payment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
