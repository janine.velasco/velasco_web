<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WebActivity_RegisteredUser</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e74ff47f-a751-4f6b-839a-0fea038975bd</testSuiteGuid>
   <testCaseLink>
      <guid>83e10d59-4e2b-4619-9451-fd37e8425db4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC01 - User can view the components of landing page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>290167c9-425b-4cc8-9228-7115afb68ba0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC02 - User can view components under Women tab upon hovering</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>175b8817-6059-4fab-967f-1dbc1dc7ddfc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC03 - User can add items to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18e57f53-ec7d-457d-960a-be15705cfad6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC05.1 - Registered user can proceed to checkout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>487e8368-2368-4f29-9a5e-35063faf5dab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E-Commerce/TC06.1 - Registered user can confirm payment</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
