<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>footer</name>
   <tag></tag>
   <elementGuidId>36cdc93d-afe2-4d37-aeaa-478020a11bfa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//footer[@id='footer']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#footer > div.row</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

	Newsletter
	
		
			
				
                
                    Ok
                
				
			
		
	



	
					
				
					Facebook
				
			
							
				
					Twitter
				
			
				                	
        		
        			Youtube
        		
        	
                        	
        		
        			Google Plus
        		
        	
                	
    Follow us




	Categories
	
		
			
												

	 
		Women
	
			
												

	 
		Tops
	
			
												

	
		T-shirts
	
	

																

	
		Blouses
	
	

									
	

																

	 
		Dresses
	
			
												

	
		Casual Dresses
	
	

																

	
		Evening Dresses
	
	

																

	
		Summer Dresses
	
	

									
	

									
	

							
										
		
	 


	
	
		Information
		
							
					
						Specials
					
				
									
				
					New products
				
			
										
					
						Best sellers
					
				
										
					
						Our stores
					
				
									
				
					Contact us
				
			
															
						
							Terms and conditions of use
						
					
																
						
							About us
						
					
													
				
					Sitemap
				
			
					
		
	
		
		
			© 2014 Ecommerce software by PrestaShop™
		
	
		


	My account
	
		
			My orders
						My credit slips
			My addresses
			My personal info
						
            		
	




	
        Store information
        
                        	
            		Selenium Framework, Research Triangle Park,
North Carolina,
USA            	
                                    	
            		Call us now: 
            		(347) 466-7432
            	
                                    	
            		Email: 
            		support@seleniumframework.com
            	
                    
    


</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;footer&quot;)/div[@class=&quot;row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//footer[@id='footer']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Practice Selenium'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automation Practice Website'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//footer/div</value>
   </webElementXpaths>
</WebElementEntity>
