<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_CheckPayment</name>
   <tag></tag>
   <elementGuidId>25bbf896-24f1-4d84-87c6-2acf4f949161</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.box.cheque-box</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='center_column']/form/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>box cheque-box</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
			Check payment
			
				
					You have chosen to pay by check. Here is a short summary of your order:
				
			
			
				- The total amount of your order comes to:
				$45.51
									(tax incl.)
							
			
				-
									We allow the following currencies to be sent by check: Dollar
					
							
			
				- Check owner and address information will be displayed on the next page.
				
				- Please confirm your order by clicking 'I confirm my order'.
			
		</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/form[1]/div[@class=&quot;box cheque-box&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/form/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div</value>
   </webElementXpaths>
</WebElementEntity>
