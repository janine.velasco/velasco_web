import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.mouseOver(findTestObject('Check out via registered email/item1'))

WebUI.click(findTestObject('Check out via registered email/btn_AddtoCart'))

WebUI.click(findTestObject('Check out via registered email/btn_ContinueShopping'))

WebUI.mouseOver(findTestObject('Check out via registered email/item2'))

WebUI.click(findTestObject('Check out via registered email/btn_AddtoCart2'))

WebUI.click(findTestObject('Check out via registered email/btn_ProceedtoCheckout'))

WebUI.click(findTestObject('Check out via registered email/btn_ProceedtoCheckout2'))

WebUI.setText(findTestObject('Check out via registered email/input_EmailAddress'), 'ja01@yopmail.com')

WebUI.setEncryptedText(findTestObject('Check out via registered email/input_Password'), 'qTHWgnaKWcPnhZ6aFERmeA==')

WebUI.click(findTestObject('Check out via registered email/btn_SignIn'))

WebUI.verifyElementVisible(findTestObject('Check out via registered email/BillingAddress'))

WebUI.verifyElementVisible(findTestObject('Check out via registered email/DeliveryAddress'))

WebUI.click(findTestObject('Check out via registered email/btn_ProceedtoCheckout2'))

WebUI.click(findTestObject('Check out via registered email/chbox_ToS'))

WebUI.click(findTestObject('Check out via registered email/btn_ProceedtoCheckout1'))

WebUI.click(findTestObject('Checkout/Sign up after adding items to cart/btn_PayByCheck'))

WebUI.verifyElementVisible(findTestObject('Confirm Payment/lbl_CheckPayment'))

WebUI.click(findTestObject('Confirm Payment/btn_ConfirmOrder'))

