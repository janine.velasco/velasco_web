import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('Landing Page/img_banner'))

WebUI.verifyElementVisible(findTestObject('Landing Page/img_subBanner'))

WebUI.verifyElementVisible(findTestObject('Landing Page/btn_ContactUs'))

WebUI.verifyElementVisible(findTestObject('Landing Page/btn_Sign in'))

WebUI.verifyElementVisible(findTestObject('Landing Page/img_Logo'))

WebUI.verifyElementVisible(findTestObject('Landing Page/input_Search'))

WebUI.verifyElementVisible(findTestObject('Landing Page/dropDown_Cart'))

WebUI.verifyElementVisible(findTestObject('Landing Page/btn_Options'))

WebUI.scrollToElement(findTestObject('Landing Page/btn_PopBestSeller'), 10)

WebUI.verifyElementVisible(findTestObject('Landing Page/btn_PopBestSeller'))

WebUI.scrollToElement(findTestObject('Landing Page/footer'), 10)

WebUI.verifyElementVisible(findTestObject('Landing Page/footer'))

