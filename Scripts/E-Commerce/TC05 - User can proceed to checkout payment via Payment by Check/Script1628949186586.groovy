import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.mouseOver(findTestObject('Sign up after adding items to cart/item1'))

WebUI.click(findTestObject('Checkout/Sign up after adding items to cart/btn_AddtoCart'))

WebUI.click(findTestObject('Sign up after adding items to cart/btn_ContShopping'))

WebUI.mouseOver(findTestObject('Sign up after adding items to cart/item2'))

WebUI.click(findTestObject('Sign up after adding items to cart/btn_AddtoCart2'))

WebUI.click(findTestObject('Sign up after adding items to cart/btn_ProceedCheckout'))

WebUI.click(findTestObject('Sign up after adding items to cart/btn_ProceedCheckout_signup'))

WebUI.setText(findTestObject('Sign up after adding items to cart/input_SignUpEmailAdd'), 'ja2010@yopmail.com')

WebUI.click(findTestObject('Sign up after adding items to cart/btn_CreateAccount'))

WebUI.click(findTestObject('Sign up after adding items to cart/rbtn_Mrs'))

WebUI.setText(findTestObject('Sign up after adding items to cart/input_FirstName'), 'Janine ')

WebUI.setText(findTestObject('Sign up after adding items to cart/input_LastName'), 'Margaret')

WebUI.setEncryptedText(findTestObject('Sign up after adding items to cart/input_Password'), 'qTHWgnaKWcPnhZ6aFERmeA==')

WebUI.selectOptionByValue(findTestObject('Object Repository/Sign up after adding items to cart/select_-12345678910111213141516171819202122_51e29d'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Sign up after adding items to cart/select_-JanuaryFebruaryMarchAprilMayJuneJul_702766'), 
    '1', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Sign up after adding items to cart/select_-20212020201920182017201620152014201_28b437'), 
    '2011', true)

WebUI.click(findTestObject('Sign up after adding items to cart/chbox_Newsletter'))

WebUI.click(findTestObject('Sign up after adding items to cart/chbox_SignUpNewsLetter'))

WebUI.setText(findTestObject('Sign up after adding items to cart/input_Company'), 'White Cloak')

WebUI.setText(findTestObject('Sign up after adding items to cart/input_Address'), 'Street Address')

WebUI.setText(findTestObject('Sign up after adding items to cart/input_City'), 'ThisIsCity')

WebUI.selectOptionByValue(findTestObject('Object Repository/Sign up after adding items to cart/select_-AlabamaAlaskaArizonaArkansasCalifor_c52141'), 
    '6', true)

WebUI.setText(findTestObject('Sign up after adding items to cart/input_PostalCode'), '12345')

WebUI.setText(findTestObject('Sign up after adding items to cart/input_MobileNumber'), '079854156952')

WebUI.setText(findTestObject('Sign up after adding items to cart/input_AliasAddress'), 'ThisIsAnAddressAlias')

WebUI.click(findTestObject('Sign up after adding items to cart/btn_Register'))

WebUI.click(findTestObject('Checkout/Sign up after adding items to cart/btn_POC3'))

WebUI.click(findTestObject('Checkout/Sign up after adding items to cart/chbox_TermsofService'))

WebUI.click(findTestObject('Checkout/Sign up after adding items to cart/btn_ProceedtoCheckout4'))

WebUI.click(findTestObject('Checkout/Sign up after adding items to cart/btn_PayByCheck'))

