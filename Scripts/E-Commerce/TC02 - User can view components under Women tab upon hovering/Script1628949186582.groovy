import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.mouseOver(findTestObject('Women Tab/opt_Womentab'))

WebUI.verifyElementVisible(findTestObject('Women Tab/lbl_Tops'))

WebUI.verifyElementVisible(findTestObject('Women Tab/link_T-shirts'))

WebUI.verifyElementVisible(findTestObject('Women Tab/link_Blouses'))

WebUI.verifyElementVisible(findTestObject('Women Tab/lbl_Dresses'))

WebUI.verifyElementVisible(findTestObject('Women Tab/link_CasualDresses'))

WebUI.verifyElementVisible(findTestObject('Women Tab/link_EveningDresses'))

WebUI.verifyElementVisible(findTestObject('Women Tab/link_SummerDresses'))

WebUI.verifyElementVisible(findTestObject('Women Tab/img1'))

WebUI.verifyElementVisible(findTestObject('Women Tab/img2'))

WebUI.closeBrowser()

