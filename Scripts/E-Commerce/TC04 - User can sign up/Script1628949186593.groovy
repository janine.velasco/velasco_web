import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.url)

WebUI.click(findTestObject('Signup/btn_Sign in'))

WebUI.setText(findTestObject('Signup/input_EmailAdd'), 'ja02@yopmail.com')

WebUI.click(findTestObject('Signup/btn_Create an account'))

WebUI.click(findTestObject('Signup/rbtn_Mrs'))

WebUI.setText(findTestObject('Signup/input_FirstName'), 'Janine')

WebUI.setText(findTestObject('Signup/input_LastName'), 'Velasco')

WebUI.setEncryptedText(findTestObject('Signup/input_Password'), 'qTHWgnaKWcPnhZ6aFERmeA==')

WebUI.setText(findTestObject('Signup/input_Address1'), 'Ganda')

WebUI.setText(findTestObject('Signup/input_City'), 'Ganda City')

WebUI.selectOptionByValue(findTestObject('Object Repository/Signup/select_-AlabamaAlaskaArizonaArkansasCalifor_c52141'), 
    '3', true)

WebUI.setText(findTestObject('Signup/input_PostCode'), '12345')

WebUI.setText(findTestObject('Signup/input_MobileNumber'), '0321551548')

WebUI.setText(findTestObject('Signup/input_SummAddress'), 'Maganda City')

WebUI.click(findTestObject('Signup/btn_Register'))

